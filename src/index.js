import _ from 'lodash'
import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import YTSearch from 'youtube-api-search'
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail';
const API_KEY = 'AIzaSyCM7tG4sdmwB7MWQWWPaLjdiun0vNjrxeg';

// Create new component This component should make some HTML:
// () => : function()
class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      videos: [],
      selectedVideo: null
    };

    this.videoSearch('hamsters');
  }

  videoSearch(term) {
    YTSearch({ key: API_KEY, term: term }, (videos) => {
      this.setState({
        videos: videos,
        selectedVideo: videos[0]
      });
    });
  }

  render() {
    const videoSearch = _.debounce((term) => { this.videoSearch(term) }, 300);

    return (
      <div>
        <SearchBar onSearchTermChange = { videoSearch }/>
        <VideoDetail video = { this.state.selectedVideo } />
        <VideoList
          videos = { this.state.videos }
          onVideoSelect = { (selectedVideo) => this.setState({ selectedVideo }) }/>
      </div>
    );
  }
}

// Take this component's HTML generated, put it on the page:
ReactDOM.render(<App />, document.querySelector('.container'));
